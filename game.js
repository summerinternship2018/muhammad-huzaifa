const canvas = document.getElementById('tetris');
const context = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

context.scale(2,2);

//context.fillStyle = '#FF0000';
//context.fillRect(0,0,1000,600);
window.alert('strike the GREEN balls and stay away from RED balls');

let myscore = 000;

let mouse = {
    x: 10,
    y: 10
}
function randomIntFromRange(min,max){
    return Math.floor(Math.random()*(max - min + 1) + min);
}
//pythagorus theoram to find distance between two points
function getDistance(x1,y1,x2,y2){
    let xDistance = x2 - x1;
    let yDistance = y2 - y1;
    
     return Math.sqrt(Math.pow(xDistance,2) + Math.pow(yDistance,2));
    
}        
document.addEventListener("mousemove",function(event){
    
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

function Circle (x,y,radius,color){
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.velocity = {
        x: Math.random()-2,
        y: Math.random()-2
    };  
    this.update = circle =>{
        
        this.draw();
        
        
        if(this.x - this.radius <= 0 || this.x + this.radius  >= 1000)
            {
                this.velocity.x = -this.velocity.x;
            }
        if(this.y - this.radius <= 0 || this.y + this.radius  >= 400)
            {
                this.velocity.y = -this.velocity.y;
            }
        this.x += this.velocity.x;
        this.y += this.velocity.y;
    };

        this.draw = function(){
        context.beginPath();
        context.arc(this.x,this.y,this.radius,0,Math.PI*2,false);
        context.strokeStyle = this.color;
        context.fillStyle = this.color;
        context.fill();
        context.closePath();
        context.stroke();

    };
};
function controlledCircle (x,y,radius,color){
    this.x = mouse.x;
    this.y = mouse.y;
    this.radius = radius;
    this.color = color;
   
    this.update = circle =>{
        
        this.draw();
     };

        this.draw = function(){
        context.beginPath();
        context.arc(this.x,this.y,this.radius,0,Math.PI*2,false);
        context.strokeStyle = this.color;
        context.fillStyle = 'black';
        context.fill();
        context.stroke();
        context.closePath();
        context.stroke();

    };
};

function collision_detect(control_c , circle){
   
    
    for(var k = 0; k<circle.length;k++)
            {
 
                if(getDistance(control_c.x,control_c.y,circle[k].x,circle[k].y) < control_c.radius + circle[k].radius)   
                    {
                    
                        if(circle[k].color ==='green'){
                        console.log(circle[k].color);
                        myscore +=10;
                        control_c.radius += 10;
                        circle[k].radius =0;
                        circle[k].x = -100;
                        circle[k].y = -100;
                        document.getElementById('p_score').innerHTML = myscore;
                        }
                    else  if (circle[k].color === 'red'){
                        
                        myscore -= 10;
                        control_c.radius -= 10;
                        circle[k].x =  randomIntFromRange(circle[k].radius, 1000 - circle[k].radius);
                        circle[k].y =  randomIntFromRange(circle[k].radius, 400 - circle[k].radius);
                        document.getElementById('p_score').innerHTML = myscore;
                    }
                    }
                else
                    {
                        console.log('no collision');
                    }
                
            }
        
}
let circle;
let controlcircle;
let level = 1;
function init()
{
    let radius = 20;
    circle = [];
    //circle.push(new Circle(undefined,undefined,radius,'brown' ));
    for (let i = 0 ; i < 15 ; i++){
        
        let x = randomIntFromRange(radius, 1000 - radius);
        let y = randomIntFromRange(radius, 400 - radius);
        var color = ['green','red'];
        var col = Math.floor(Math.random()*color.length);
        if(i !== 0)
        {
            for (let j = 0 ; j < circle.length ; j++)
                {
                    if(getDistance(x,y,circle[j].x,circle[j].y) - radius * 2 < 0)
                        {
                            x = randomIntFromRange(radius, 1000 - radius);
                            y = randomIntFromRange(radius, 400 - radius);
                            
                            j = -1;
                        }
                }
        }
                circle.push(new Circle(x,y,radius,color[col]));
    
}
    
controlcircle = new controlledCircle(undefined,undefined,radius,'black');
    }

function animate()
{
    
    requestAnimationFrame(animate);
    context.clearRect(0,0,canvas.width,canvas.height);
    for(let i=1 ; i < circle.length ; i++)
        {
            circle[i].update(circle);
        }
    controlcircle.x = mouse.x;
    controlcircle.y = mouse.y;
    console.log(controlcircle);
    controlcircle.update();
    collision_detect(controlcircle,circle);
    if(myscore > 60){
        window.alert('you won');
        document.reload();
    }
}
    
  
init();
animate();
//console.log(circle);

